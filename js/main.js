let commandPointsOnDay = [3, 4, 4, 5, 6, 2, 3, 5];
let tasksPoints = [12, 9, 18, 12];
let deadlineDate = new Date(2019, 6, 5);
let nowDate = new Date( );
let now = nowDate.getFullYear() + '.' + (nowDate.getMonth() + 1) + '.' + nowDate.getDate(); 

let sumPointsOnDay = 0; // определяем количество поинтов которые команда выполняет за день
for (let points of commandPointsOnDay){
    sumPointsOnDay += points;
}

let sumTasksPoints = 0; // определяем общее количество поинтов в заданиях проекта
for(let points of tasksPoints){
    sumTasksPoints += points;
}

let nessesaryDays = Math.ceil(sumTasksPoints/sumPointsOnDay); // определяем необходимое количество дней

// создаем функцию, которая считывает количество рабочих дней между двумя произвольными датами включая их
function calcWorkingDays(now, deadlineDate){// принимает параметры текущая дата и контрольная дата
  if (now > deadlineDate) return -1;// окончание функции если контрольная дата меньше текущей
// вычисляем количество рабочих дней первой и последей недель
let firstWeekDay = nowDate.getDay();// получаем день недели текущей даты
let firstWeekWorkDays; // создаем переменную которой присваиваем количество рабочих дней первой недели
if (firstWeekDay == 0 || firstWeekDay == 6){
  firstWeekWorkDays = 0;
} else {
  firstWeekWorkDays = 7 - 2 - (firstWeekDay - 1); // от полной недели отнимаем 2 выходных и 
                                                  // (порядковый номер текущего дня - 1), т.к. первый день тоже включаем.
}

let lastWeekDay = deadlineDate.getDay()// получаем день контрольной даты
let lastWeekWorkDays // создаем переменную которой присваиваеваем количество рабочих дней последенй недели
if(lastWeekDay < 6 && lastWeekDay > 0 ) lastWeekWorkDays = lastWeekDay;
else if(lastWeekDay == 6 || lastWeekDay == 0) lastWeekWorkDays = 5; // если контрольный день выпадает на субботу
                                                                    // или воскресенье то рабочих дней 5

// определяем количество дней в первой и последней неделях

let fullFirstWeekDays = 7 - firstWeekDay + 1;
if (firstWeekDay == 0) fullFirstWeekDays = 1;

let fullLastWeekDays = lastWeekDay;
if(lastWeekDay == 0) fullLastWeekDays = 7;

// определяем количество полных недель, за исключением первой и последней неделию
let numAllDays = (deadlineDate - new Date(now))/1000/3600/24 + 1;//так как дата без времени устанавливается с 00 часов
let numFullWeeks = (numAllDays - (fullFirstWeekDays + fullLastWeekDays))/7;

let numWorkDays = numFullWeeks * 5 + firstWeekWorkDays + lastWeekWorkDays; //количество полных недель умножаем на 
                                                                       //5 рабочих дней и прибавляем рабочие дни первой
                                                                       // и последней недель  

 return (numWorkDays) ;                                                                     
};

let daysForDeadline = calcWorkingDays(now, deadlineDate);
console.log(daysForDeadline);

  let result;
  if (nessesaryDays - daysForDeadline < 0 ) {
      result = `Все задачи будут успешно выполнены за ${daysForDeadline - nessesaryDays} дней до наступления дедлайна!`;
  } else if(nessesaryDays - daysForDeadline == 0) {
    result = `Все задачи будут успешно выполнены к дедлайну!`;
  } else if (nessesaryDays - daysForDeadline > 0){
    result = `Команде разработчиков придется потратить дополнительно ${(nessesaryDays - daysForDeadline) * 8} часов после дедлайна, чтобы выполнить все задачи в беклоге`;
  }

  console.log(result);

